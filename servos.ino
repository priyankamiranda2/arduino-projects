#define BLYNK_PRINT Serial
#include <BlynkSimpleSerialBLE.h>
#include <SoftwareSerial.h>
#include <Servo.h>
char auth[] = "2033a7581c2242629a35cb546dc840ee";
SoftwareSerial SerialBLE(10, 11);
Servo Servo1, Servo2, Servo3; // create servo object to control a servo
int i, time, obstacle;
int pos1, pos2, pos3; 
int pos1R, pos2R, pos3R; 
int phase=45;
int velocity=2000; 
int maxDeflexion=20; 
int maxDefobs=20; 
int actualTime;
float shift;
const int center1=98; 
const int center2=90;
const int center3=105;
const int lostTime=3000;
int analogPin = 0;     // Infrared Sensor (Right lead) connected to analog pin 0
 int input;                                   // outside leads to ground and +5V
int val = 0;
BLYNK_WRITE(V3)
{
 Servo1.write(param.asInt()*180);
 //Servo2.write(param.asInt()*180);
}

void setup()
{
   Blynk.begin(SerialBLE, auth);
  Serial.println("Waiting for connections...");
Servo1.attach(5); 
Servo2.attach(6); 
time=velocity/360;
shift=0;

}

void loop()
{
  
val = analogRead(analogPin); 
for (i=0; i<360; i++) {
pos1 = i+2*phase;
pos2 = i+phase;
if (pos1>359) pos1-=360;
if (pos2>359) pos2-=360;
if (pos1>179) pos1=360-pos1; 
if (pos2>179) pos2=360-pos2;
pos1R=map(pos1,0,180,center1-maxDeflexion-obstacle,center1+maxDeflexion-obstacle);
pos2R=map(pos2,0,180,center2-maxDeflexion-obstacle,center2+maxDeflexion-obstacle);
Servo1.write(pos1R); 
Servo2.write(pos2R);  
delay(time);

obstacle=int(shift);
Blynk.run();
if (input ==1) { 
if (obstacle<maxDefobs) shift=shift+0.05; 
actualTime=millis();
}
if (input==2) { 
if (obstacle > (-maxDefobs)) shift=shift-0.05;
actualTime=millis();
}
else{
if (millis()>actualTime+lostTime) {
if (shift>0) shift=shift-0.05;
if (shift<0) shift=shift+0.05;
}
}

}
}
