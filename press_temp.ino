#include <SoftwareSerial.h>

#include <Servo.h> 
Servo servo1,servo2,servo3;   
 
boolean left=0, right=0;
#include <OneWire.h>
OneWire  ds(4);  // on pin 10 (a 4.7K resistor is necessary)
SoftwareSerial mySerial(10, 11); // RX, TX
volatile int  flow_frequency;  // Measures flow meter pulses
unsigned int  l_hour;          // Calculated litres/hour                      
unsigned char flowmeter = 2;  // Flow Meter Pin number
unsigned long currentTime;
unsigned long cloopTime;
void flow ()                  // Interruot function
{ 
   flow_frequency++;
} 

void setup()
{  Serial.begin(9600);
   pinMode(flowmeter, INPUT);
   Serial.begin(9600); 
    mySerial.begin(9600);
     sei();          
   attachInterrupt(0, flow, RISING);                                 
   sei();                          
   currentTime = millis();
   cloopTime = currentTime;
   servo1.attach(5);  //first servo 
 servo2.attach(6);  //second servo 
} 

void loop ()    
{
    int pos = 90;    
int pos2 = 90; 
  servo1.write(pos); 
    servo2.write(pos2);
       while(pos<135)                  // mid body starts moving left
  {                                 
    servo1.write(pos);
    pos++;            
    delay(5);                    
    }
   while(pos<160)                // next part starts moving left
  {                                 
    servo1.write(pos); 
    servo2.write(pos2);
    pos++;  
    pos2++;        
    delay(5);                 
    }
      while(pos2<160)            // last part starts moving right
  {                                  
    servo2.write(pos2);
    pos2++;        
     delay(5);                    
    }
   
                                        //left tail movement done(now mid body is at 180, next part is at 180, tail is at 0)
                                   
while(pos>135)                  // mid body starts moving towards centre
  {                                 
    servo1.write(pos);
    pos--;            
     delay(5);                    
    }
   while(pos>90)                // next part starts moving towards centre
  {                                 
    servo1.write(pos); 
    servo2.write(pos2);
    pos--;  
    pos2--;        
     delay(5);                    
    }
      while(pos2>90)            // last part starts moving towards centre
  {                                  
    servo2.write(pos2);
    
    pos2--;        
    delay(5);                    
    }
   

                                  //tail movement towards centre is done(now mid body is at 0, next part is at 0, tail is at 0)

while(pos>45)                  // mid body starts moving left
  {                                 
    servo1.write(pos);
    pos--;            
    delay(5);                    
    }
   while(pos>20)                // next part starts moving left
  {                                 
    servo1.write(pos); 
    servo2.write(pos2);
    pos--;  
    pos2--;        
    delay(5);                    
    }
      while(pos2>20)            // last part starts moving right
  {                                  
    servo2.write(pos2);
  
    pos2--;        
   delay(5);                    
    }
   

while(pos<45)                  // mid body starts moving towards centre
  {                                 
    servo1.write(pos);
    pos++;            
    delay(5);                    
    }
   while(pos<90)                // next part starts moving towards centre
  {                                 
    servo1.write(pos); 
    servo2.write(pos2);
    pos++;  
    pos2++;        
    delay(5);                    
    }
      while(pos2<90)            // last part starts moving towards centre
  {                                  
    servo2.write(pos2);
 
    pos2++;        
    delay(5);                    
    }
   

   currentTime = millis();
   if(currentTime >= (cloopTime + 1000))
   {     
      cloopTime = currentTime;                   
      l_hour = (flow_frequency * 60 / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flow rate in L/hour 
      flow_frequency = 0;     
       mySerial.print("Water flow=");                   
      mySerial.print(l_hour, DEC);            
      mySerial.println(" L/hour");
   }
  byte i;   
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
    return;
  } 
      type_s = 0;

  ds.reset();
  ds.select(addr);
  ds.write(0x44);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
 
  }

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  mySerial.print("  Temperature = ");
  mySerial.print(celsius);
  mySerial.println(" Celsius ");
}
