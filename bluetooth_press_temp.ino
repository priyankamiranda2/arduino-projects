
#include <SoftwareSerial.h>
#define BLYNK_PRINT Serial
#include <Servo.h>

#include <BlynkSimpleSerialBLE.h>

#include <SimpleTimer.h>
SoftwareSerial SerialBLE(10, 11); // RX, TX
Servo Servo1, Servo2;
int i1, time, obstacle;
int pos1, pos2, pos3; 
int pos1R, pos2R, pos3R; 
int phase=45;
int velocity=2000; 
int maxDeflexion=20; 
int maxDefobs=20; 
int actualTime;
float shift;
const int center1=98; 
const int center2=90;
const int center3=105;
const int lostTime=3000;
int analogPin = 0;     // Infrared Sensor (Right lead) connected to analog pin 0
                                  // outside leads to ground and +5V
int val = 0;
SimpleTimer timer;
#include <OneWire.h>
char auth[] = "2033a7581c2242629a35cb546dc840ee";
OneWire  ds(4);  // on pin 10 (a 4.7K resistor is necessary)
    char input;  
      unsigned long currentTime;
unsigned long cloopTime;
volatile int  flow_frequency;  // Measures flow meter pulses
unsigned int  l_hour;          // Calculated litres/hour                      
unsigned char flowmeter = 2;  // Flow Meter Pin number
    byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
  BLYNK_WRITE(V2)
{
  Servo1.write(param.asInt()*180);
}


 void sendUptime()
    {
     // shows the value temp on virtual pin 10
      
    }
void flow ()                  // Interruot function
{ 
   flow_frequency++;
} 

    void setup() 
    {  
      Serial.begin(9600); 
        pinMode(flowmeter, INPUT);
   Serial.begin(9600); 
   attachInterrupt(0, flow, RISING); // Setup Interrupt 
                                     // see http://arduino.cc/en/Reference/attachInterrupt
   sei();                            // Enable interrupts  
   currentTime = millis();
   cloopTime = currentTime;
      Serial.println(">> START<<");                                      

 
timer.setInterval(1000L, sendUptime);
  Serial.println("Waiting for connections...");

  // Setup a function to be called every second
Serial.begin(9600);
Servo1.attach(5); 
Servo2.attach(6); 
time=velocity/360;
shift=0;
 Blynk.begin(SerialBLE, auth);
    }  
      
    void loop() 
    {  
 
  if ( !ds.search(addr)) {
 //   Serial.println("No more addresses.");
    Serial.println();
    ds.reset_search();
    delay(250);
    return;
  }

  if (OneWire::crc8(addr, 7) != addr[7]) {
      Serial.println("CRC is not valid!");
      return;
  }
  Serial.println();
 
  // the first ROM byte indicates which chip
  switch (addr[0]) {
    case 0x10:
 //     Serial.println("  Chip = DS18S20");  // or old DS1820
      type_s = 1;
      break;
    case 0x28:
  //    Serial.println("  Chip = DS18B20");
      type_s = 0;
      break;
    case 0x22:
 //     Serial.println("  Chip = DS1822");
      type_s = 0;
      break;
    default:
   //   Serial.println("Device is not a DS18x20 family device.");
      return;
  } 

  ds.reset();
  ds.select(addr);
  ds.write(0x44);        // start conversion, with parasite power on at the end
  
  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.
  
  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

//  Serial.print("  Data = ");
//  Serial.print(present, HEX);
//  Serial.print(" ");
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    Serial.print(data[i], HEX);
    Serial.print(" ");
  }
//  Serial.print(" CRC=");
//  Serial.print(OneWire::crc8(data, 8), HEX);
//  Serial.println();

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = (float)raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  Serial.print("  Temperature = ");
  Serial.print(celsius);
  Serial.print(" Celsius, ");
  Serial.print(fahrenheit);
  Serial.println(" Fahrenheit");
        
      
           currentTime = millis();
   // Every second, calculate and print litres/hour
   if(currentTime >= (cloopTime + 1000))
   {     
      cloopTime = currentTime;              // Updates cloopTime
      // Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min. (Results in +/- 3% range)
      l_hour = (flow_frequency * 60 / 7.5); // (Pulse frequency x 60 min) / 7.5Q = flow rate in L/hour 
      flow_frequency = 0;                   // Reset Counter
      Serial.print(l_hour, DEC);            // Print litres/hour
      Serial.println(" L/hour");
      val = analogRead(analogPin); 
for (i1=0; i1<360; i1++) {
pos1 = i1+2*phase;
pos2 = i1+phase;
if (pos1>359) pos1-=360;
if (pos2>359) pos2-=360;
if (pos1>179) pos1=360-pos1; 
if (pos2>179) pos2=360-pos2;
pos1R=map(pos1,0,180,center1-maxDeflexion-obstacle,center1+maxDeflexion-obstacle);
pos2R=map(pos2,0,180,center2-maxDeflexion-obstacle,center2+maxDeflexion-obstacle);
Servo1.write(pos1R); 
Servo2.write(pos2R);  
delay(time);

obstacle=int(shift);
      Blynk.begin(SerialBLE, auth);
   Blynk.run();
    timer.run(); // Initiates SimpleTimer
  Blynk.virtualWrite(V0, millis()); 


  
         }
        
   }     
    }  
